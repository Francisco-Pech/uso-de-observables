import { Component } from '@angular/core';
import { Observable, Observer, of, pipe, fromEvent } from 'rxjs';
import { map, tap, delay, scan, filter } from 'rxjs/operators';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {

  constructor(){

    /**
     * Creación de observer, este imprime los datos respectivos en caso de corraborar la existencia de un número,
     * Si no es un número y es un error entonces lo imprime, en caso de terminar el trabajo lo señala
     */
    const myObserver: Observer<any> = {
      next: x => {
        if(!isNaN(x)){
          console.log(x);
        }else{
          console.log(`'${x}': No es un número`);
        }
      },
      error: e => console.log(`Error: `, e),
      complete: () => console.log('Trabajo completo')
    }

    /**
     * El observable depende de los observer, en este caso imprimos los datos despectivos a la estructura
     * rxj, cuando se imprime un error o bien un complete se termina de ejecutar el observable
     */
    const myObservable = new Observable(
      subscriber => {
        subscriber.next('Prueba de número');
        subscriber.next(1);
        subscriber.error('Soy un error');
      }   
    );

    /**
     * Como se tiene un error en el observable previo, se tiene que crear uno nuevo. No se puede poner otros
     * Valores después de complete o error
     */
    const myObservable2 = new Observable(
      subscriber => {
        subscriber.complete();
      }   
    );

    /**
     * Se llaman los respectivos observables con su respectivo observer
     */
   // myObservable.subscribe(myObserver);
   // myObservable2.subscribe(myObserver);


   /**
    * El uso de pipe permite manejar operadores de manera interna, aun no se le pasa ningún valor u observer
    * es una fase previa a ello.
    * Una vez realizado los operadores deseados se subscribe
    */
   const pipeOne = myObservable.pipe(
    // Filtramos los valores, evitando que entre otra cosa que no sea número, si es string no se visualiza
    // el valor dentro del observer
    filter((r:any) => !isNaN(r)),
    // Una vez realizado el filtro este se pasa por map, que tiene como entrada un dato y retorna el mismo
    // dato
    map((r:any) => {
      return  r;
    })
   );

   /** 
   * Se puede manerar un observable con operadores en una variable, en donde se subscribe un observer
   */
  // pipeOne.subscribe(myObserver);

  /**
   * La funcionalidad de of es crear un observable individual, este recibe solo un dato. Puede ser igual
   * un observer
   */
  const source = of("World").pipe(
    // Por su parte map hace uso de la misma variable de ingreso "World" y lo maneja, regresandolo 
    // para su uso, en este caso se concatena
    map(x => `Hello ${x}!`),
    // El tap por su parte maneja la subscripción previa y la modifica en caso de que faltara algo
    tap(te => console.log(te + " TAP")),
    // El delay como su nombre lo indica espera un tiempo, esta esta en ms
    delay(2000),
    // El scan por su parte hace uso de 2 parámetros, el primero es el valor de la subscripción previa
    // sin la modificación del tap y el segundo es un valor que se le añade, es similar al tap, pero
    // este maneja dos valores
    scan((acc,one) => acc + one, "SCAN "),
    // Filter como se menciono antes filtra solo los valores que cumplan sus restricciones
    filter(x => x.includes("Hello"))
  );


  /**
   * Imprime el valor de source, siendo esta un pipe, recordamos que pipe no se puede modificar internamente
   * con diferencia al observer que si se puede
   */
  //source.subscribe(console.log);

  /**
   * Se crea una variable que contiene una función que hace uso de un pipe
   */
  const toogle = () => 
  // El pape hace la misma función que la que hace con algun observer, la diferencia es que este es para
  // una función
  pipe(
    // Hacemos uso del scan, en donde recordemos el segundo valor, es decir, value es el valor que podemos
    // modificar
    scan((acc, value:any) =>{
      // Creamos una neuva variable local, que concatene el arreglo de fakeData
      const newValue = value.a;
        // Condición para determinar unicamente valores pares
        if(newValue % 2 === 0){
          // Si es par entonces en el valor que tiene actualmente acc se le agrega los valors de value con
          // el arreglo fakeData
          acc.push(newValue);
        }
        return acc;
    }, []),  // Se le pone [] debido a que necesitamos inicializar el pipe como un arreglo
    // El tap en esta ocasión guarda el valor de value, sin embargo, hace uso del arreglo el cual imprime
    tap((v) => console.log(v))
  );

    /**
     * Realizamos un arregloc on los valores que deseamos visualizar
     */
  const fakeData = [
    {a: 1},
    {a: 2},
    {a: 3},
    {a: 4}
  ]

  /**
   * Pasamos el valor del arreglo el cual lo interpreta como un observable
   */
  const source2 = of(...fakeData).pipe(
    toogle(),                       // Llamamos a la función para hacer que funcione, recordemos imprime un arreglo
    map(x => `Hello ${x}!`)
  );

  //source2.subscribe(console.log);
/*
 const click$ = fromEvent(document, 'click');

    
    click$
      .pipe(
        map((data) => {
          if(data.isTrusted){
            return 10;
          }
        })

      ).subscribe(console.log)

      */
  }



  title = 'observables-base01';
}
