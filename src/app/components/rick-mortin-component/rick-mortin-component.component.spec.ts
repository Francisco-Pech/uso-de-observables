import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RickMortinComponentComponent } from './rick-mortin-component.component';

describe('RickMortinComponentComponent', () => {
  let component: RickMortinComponentComponent;
  let fixture: ComponentFixture<RickMortinComponentComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RickMortinComponentComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RickMortinComponentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
