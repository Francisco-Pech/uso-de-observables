import { Component, OnInit } from '@angular/core';
import { RickMortinService } from '../../services/rick-mortin/rick-mortin.service';
import { RickMortin } from '../../interfaces/rick-mortin/rick-mortin';
import { SelectRickMortin  } from '../../interfaces/select-rick-mortin/select-rick-mortin';
import { Observable, forkJoin } from 'rxjs';
//import { Observable, Subscribable ,SubscribableOrPromise , ObservableInput } from 'rxjs';
//import { Observer, NextObserver, ErrorObserver, CompletionObserver, PartialObserver } from 'rxjs';
//import { Operator, Subscriber, Subscription, SubscriptionLike, TeardownLogic } from 'rxjs';
//import { SchedulerAction, SchedulerLike } from 'rxjs';
import { mergeMap } from 'rxjs/operators';
import { ajax, AjaxError } from 'rxjs/ajax';

@Component({
  selector: 'app-rick-mortin-component',
  templateUrl: './rick-mortin-component.component.html',
  styleUrls: ['./rick-mortin-component.component.css']
})
export class RickMortinComponentComponent implements OnInit {

  //name$ = Observable;
  allCharacter: [];

  constructor(
    private RickMortinServices: RickMortinService
  ) { 
    //this.name$ = this.RickMortinServices.getMapCharacterWithAngular(1);
  }

  ngOnInit(): void {
  //  this.getAllCharacter();
   // this.RickMortinServices.getCharacterWithMap();
    //this.RickMortinServices.useSkipUntilAndShareReplay();
    //this.RickMortinServices.useCatchError();
    //this.RickMortinServices.useSubject();
    //this.getUniqueCharacter();
    //this.getUseForkJoin();
    this.getMargeMap();
  }

  getAllCharacter = () =>{
    this.RickMortinServices.getCharacter().subscribe((response: any)=>{
      console.log(response);
      this.allCharacter =  response.results;
    }, (error) => {
      alert("El error es: " + error.statusText);
    });
  }

  /**
   * Hacemos uso de map y ajax para la obtención de datos
   */
  getUniqueCharacter = () =>{
    this.RickMortinServices.getMapCharacterWithAngular(1).subscribe((response: any) =>{
      console.log(response);
    })
  }

  /**
   * Hacemos uso de jorkJoin, se usa en paralelo
   */
  getUseForkJoin = () =>{
    // El forkJoin nos permite hacer la busqueda de diversos datos especificos dato los observables
    // en donde nos devuelve un arreglo con todo lo que deseabamos buscar
    forkJoin(
      this.RickMortinServices.getMapCharacterWithAngular(1),
      this.RickMortinServices.getMapCharacterWithAngular(2),
      this.RickMortinServices.getMapCharacterWithAngular(3)
    ).subscribe(
      (response) =>{
        console.log(response);
      });
  }

  /**
   * Realizamos la verificación de lo obtenido con la API sea correcto mediante mergeMap, se usa en serie
   */
  getMargeMap = () =>{
    // Obtenemos el valor otorgado por el servicio en este caso es un observer
    this.RickMortinServices.getMapCharacterWithAngular(1).pipe(
      // Utilizamos el valor anterior del observer para manejarlo en mergeMap y verificar
      // si efectivamente es correcta o no el EndPoint que manejamos
      mergeMap((res: any) => ajax(res.url)),
    ).subscribe((final: any) =>{
      // Imprime el estatus si la url se utilizo de manera correcta o no
      console.log(final.status);
    });
  }
}
