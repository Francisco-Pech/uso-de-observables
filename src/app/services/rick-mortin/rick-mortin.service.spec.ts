import { TestBed } from '@angular/core/testing';

import { RickMortinService } from './rick-mortin.service';

describe('RickMortinService', () => {
  let service: RickMortinService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(RickMortinService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
