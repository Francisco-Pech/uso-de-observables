import { Injectable } from '@angular/core';
import { RickMortin } from '../../interfaces/rick-mortin/rick-mortin';
import { SelectRickMortin } from '../../interfaces/select-rick-mortin/select-rick-mortin';
import { HttpClient,  HttpHeaders} from '@angular/common/http';
import { interval, fromEvent, of, BehaviorSubject, ReplaySubject, forkJoin, Observable } from 'rxjs';
import { ajax } from 'rxjs/ajax';
import { map, mergeMap , skipUntil, shareReplay, catchError} from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class RickMortinService {

  API = "https://rickandmortyapi.com/api/character/";
  /**
   * Hacemos uso de http para el llamado de los Endpoints
   * @param http propiedad propia de angular
   */
  constructor(
    private http: HttpClient
  ) { 
    this.http = http;
  }

  /** 
   * Obtenemos todos los personajes 
   */
  getCharacter = () =>{
    const HeadersForPatientsAPI = new HttpHeaders({
      'Content-Type': 'application/json'
    });
    return this.http.get<RickMortin>("https://rickandmortyapi.com/api/character", { headers: HeadersForPatientsAPI });
  }

  /**
   * Prueba de Map con angular para servicios
   */
  getMapCharacterWithAngular = (id:number): any =>{
    const response = ajax.getJSON(this.API + id);

    const data$ = new Observable(observer =>{
      response.subscribe(
        (resp) =>{
          observer.next(resp);
          observer.complete();
        },
        (error) =>{
          observer.error(error);
        }
      )
    });
    return data$;
  }


  /**
   * Haciendo uso de Map
   */
  getCharacterWithMap = () =>{
    // Hacemos uso del evento click, recordemos que un observer siempre es un evento, el cual se interpreta
    // normalmente con $
    const click$ = fromEvent(document, 'click');

    // Hacemos uso del observer
    click$
      .pipe(
        // Renderizamos los datos a utilizar, en este caso estamos imprimiendo un isTrusted al
        // subscribirse
        map((data) => {
          // Validamos subscripción
          if(data.isTrusted){
            // Valor que se renderiza, esto es debido a lo que se pasa a la subscripción
            return 1;
          }
        }),
        // Hacemos uso de mergeMap, el cual hace uso de un map y retorna el tipo de dato que queremos
        // utilizar, en este caso lo vemos junto a un EndPoint
        mergeMap((id)=> ajax.getJSON(`${this.API}${id}`)),
        // Se se anexa otro map, renderizamos lo obtenido en mergeMap, lo cual podemos imprimir
        map((data:any) => {
          return `Datos: ${data.name}`
        })
      ).subscribe(console.log)
  }

  /**
   * Uso del skipUntil y ShareReplay
   */
  useSkipUntilAndShareReplay = () =>{

    // Hacemos uso de un evento que repite en un intervalo de tiempo los datos que se desean,
    // se repite cada segundo
    const interval$ = interval(1000);

    // Hacemos uso del evento click, recordemos que un observer siempre es un evento, el cual se interpreta
    // normalmente con $
    const click$ = fromEvent(document, 'click');

    /**
     * Hacemos uso del observer interval$ y click$, skypUntil por su parte realiza una acción después de
     * realizar un click, si no sucede no hace nada
     */
    const emitAfterClick = interval$.pipe(skipUntil(click$));
  
    // Subscribimos una impresión en consola del emitAfterClick
    emitAfterClick.subscribe(v => console.log(v));

    /**
     * Hacemos uso de shareReplay que dividiremos en 2 partes para ver su funcionalidad, esta primera parte
     * imprime en pantalla siempre un valor, no espera ningún click o evento
     */
    const emitAndShare = interval$.pipe(shareReplay());

    // Imprime el operator ShareReplay
    emitAndShare.subscribe(v => console.log(`ShareReplay ${v}`));

    /**
     * En el caso de utilizar el click se imprime de igual manera, pero a partir de donde contaba la 
     * primera parte, al dar un click
     */
    click$.subscribe(() =>{
      emitAndShare.subscribe(v => console.log(`ShareReplay_2 ${v}`));
    });

  }

  /**
   * Hacemos uso de catchError
   */
  useCatchError = () =>{

    /**
     * Hacemos uso del observer que cuenta con 3 valores
     */
    of("Welcome Gente", "como estan?", "Hola Gente").pipe(
      // Realizamos el mapeo que como sabemos renderiza los datos de entrada
      map((v) => {
        // En este caso omitimos el caso de Hola Gente
        if(v === "Hola Gente"){
          // Guardamos el error en throw para que lo utilice catchError
          throw `${v}`;
        }
        return v;
      }),
      // catchError por su parte hace uso del error anterior previsto por la sentencia, y le añade la 
      // palabra Error
    catchError((error) => {
      throw "Error: " + error;
    })
    ).subscribe(
      // Al subscribirse se imprimen los datos renderizados del map y también los de catchError,
      // pero recordemos que carchError cuenta con una concatenación previa
      x => console.log(`Next: ` + x),
      error => console.log(error)
    );
  }

  /**
   * Hacemos uso de subject
   */
  useSubject = () =>{

    /**
     * Hacemos uso de BehaviorSubject que obtiene el último valor unicamente de lo ingresado
     */
    const saludoBehavior = new BehaviorSubject('');

    /**
     * Hacemos uso de ReplaySubject que regresa todos los valores metidos, muy similar a la acción
     * de renderizar de map
     */
    const saludoReplay = new ReplaySubject();

    // Imprimimos todos los valores que se tienen para visualizar cuales valores tenemos y comprobar
    // que efectivamente el último dato es el que obtenemos
    saludoBehavior.subscribe(
      x => {
        console.log(`Primero `, x);
      })

    // Llamamos a of para ingresar los valores deseados a saludoBehavior
    of(1,2,3,4,5,6,7).subscribe(
      v => {
        saludoBehavior.next(`${v}`);
        saludoReplay.next(`${v}`)
      })

    // Imprimimos el valor al subcribirnos
    saludoBehavior.subscribe(x=>{
      console.log(x);
    })

    saludoReplay.subscribe(x =>{
      console.log(x);
    })

   


  }

}
